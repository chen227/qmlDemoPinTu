﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QPoint>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QMenu>
#include <QAction>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void setSystemMenu();
    void readImg();
public slots:
    int getDesktopWidth();
    int getDesktopHeight();

    void systemExit(bool b);

    QString getCurrentPath();

signals:
    void initLeftList(QVariantList list);
private:
    Ui::Widget *ui;
    QPoint offset;
    QSystemTrayIcon *trayIcon;

    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);
};

#endif // WIDGET_H
