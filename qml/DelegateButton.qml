﻿import QtQuick 2.0

Item {
    id: delegate
    width: name.width
    height: name.height

    Image {
        id: name
        source: imgSource == undefined ? "" : imgSource
        width: 100
        height: 100
        fillMode: Image.Stretch
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            clickLeftList(name.source.toString())
        }
    }
}
