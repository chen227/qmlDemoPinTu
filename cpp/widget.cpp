﻿#include "widget.h"
#include "ui_widget.h"
#include <QQuickWidget>
#include <QPixmap>
#include <QPalette>
#include <QColor>
#include <QQmlEngine>
#include <QQmlContext>
#include <QDate>
#include <QSettings>
#include <QDir>
#include <QTextStream>
#include <QDesktopWidget>
#include <QApplication>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    //==================================================================================

    this->setWindowFlags(this->windowFlags() );//窗口置顶，无边框
    this->setWindowTitle("拼图游戏");

//    this->setAttribute(Qt::WA_TranslucentBackground);

//    this->setAutoFillBackground(true);//填充背景

    //================================================================================
//    QPalette palette;

//    QPixmap pixmap(":/image/toumingBg.png");

//    palette.setBrush(QPalette::Window,QBrush(pixmap));//设置笔刷为背景图片

//    this->setPalette(palette);

//    this->setFixedSize(getDesktopWidth(),getDesktopHeight());

    //================================================================================
//    ui->quickWidget->setClearColor(QColor(Qt::transparent));    //qml 透明

    ui->quickWidget->engine()->rootContext()->setContextProperty("widget",this);

    ui->quickWidget->setSource(QUrl("qrc:/qml/main.qml"));

    this->setWindowState(Qt::WindowMaximized);

//    this->setFixedSize(getDesktopWidth(),getDesktopHeight()-40);

    setSystemMenu();

    readImg();
}

Widget::~Widget()
{
    delete trayIcon;
    delete ui->quickWidget;
    delete ui;
}

int Widget::getDesktopWidth()
{
    return QApplication::desktop()->width();
}

int Widget::getDesktopHeight()
{
    return QApplication::desktop()->height();
}
void Widget::mousePressEvent(QMouseEvent *event)
{
//    if(event->button()==Qt::LeftButton){
//        offset = event->globalPos() - pos();
//    }
}
void Widget::mouseMoveEvent(QMouseEvent *event)
{
//    if(event->buttons()&Qt::LeftButton){
//        QPoint temp;
//        temp = event->globalPos() - offset;
//        move(temp);
//    }
}

void Widget::mouseReleaseEvent(QMouseEvent *event)
{

}
//添加系统托盘
void Widget::setSystemMenu()
{
    trayIcon = new QSystemTrayIcon();

    trayIcon->setIcon(QIcon(":/imamges/image/logo.ico"));

    QMenu *menu = new QMenu();

    QAction *action_exit = menu->addAction("退出");

    connect(action_exit,SIGNAL(triggered(bool)),this,SLOT(systemExit(bool)));

    trayIcon->setContextMenu(menu);

    trayIcon->show();
}

void Widget::readImg()
{
    QDir dir(QDir::currentPath()+"/data");
    QStringList nameFilters;
    nameFilters<<"*";
    QVariantList list;

    QFileInfoList infolist = dir.entryInfoList(nameFilters,QDir::NoDotAndDotDot | QDir::AllEntries,QDir::NoSort);

    for (int i = 0; i < infolist.size(); ++i) {
        QFileInfo fileInfo = infolist.at(i);
//        qDebug()<<fileInfo.fileName()<<fileInfo.filePath();
        list.append(QVariant("file:///" + fileInfo.filePath()));
    }
    emit initLeftList(list);
}
void Widget::systemExit(bool b)
{
    qApp->quit();
}

QString Widget::getCurrentPath()
{
    return QDir::currentPath();
}
