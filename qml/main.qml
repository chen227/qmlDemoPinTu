﻿import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 1.4


Item {
    id: root
    width: Screen.width
    height: Screen.height

    property int type: 3
    property int imgWidth: 0
    property int imgHeight: 0
    property var items: new Array
    property int currentDrag: 0
    property real startX: 0
    property real startY: 0
    property int lastIndex: 0
    property bool isZuoBi: false

    function addZero(value){
        if(value < 10){
            return "0" + value
        }
        return value
    }

    function getSeed(){

        var date = new Date
        var y = date.getFullYear()
        var M = addZero(date.getMonth())
        var d = addZero(date.getDate())
        var h = addZero(date.getHours())
        var m = addZero(date.getMinutes())
        var s = addZero(date.getSeconds())

        var time = String(y) + String(M) + String(d) + String(h) + String(m) + String(s)

        return Number(time)
    }


    function calcIndex(centerx,centery){

        var index = -1

        var x = centerx - rectBg.x
        var y = centery - rectBg.y

        if(x >= rectBg.width || y >= rectBg.height
                || x < 0 || y < 0){
            return index
        }

        var line = Math.floor(x / listView.cellWidth)
        var column = Math.floor(y / listView.cellHeight)

        if(line >= type) line = type - 1
        if(column >= type) column = type - 1

        index = line + column * type

        return index
    }

    function calcCenter(centerx,centery){

        //clear highlight
        for(var i=0;i<listModel.count;i++){
            listView.currentIndex = i
            listView.currentItem.rectHighlight.opacity = 0.3
        }

        var index = calcIndex(centerx,centery)
        if(index < 0) return

        listView.currentIndex = index
        listView.currentItem.rectHighlight.opacity = 0.8
    }

    function calcEnd(centerx,centery){

        //clear highlight
        for(var i=0;i<listModel.count;i++){
            listView.currentIndex = i
            listView.currentItem.rectHighlight.opacity = 0.3
        }

        var index = calcIndex(centerx,centery)

        if(index < 0) return true


        listView.currentIndex = index

        if(listView.currentItem.hasValue == false){
            listView.currentItem.myIndex = currentDrag
            listView.currentItem.img.opacity = 1
            listView.currentItem.hasValue = true

            if(lastIndex !== index && lastIndex != -1){
                listView.currentIndex = lastIndex
                listView.currentItem.hasValue = false
            }

            items[currentDrag].visible = false
            items[currentDrag].x = rectBg.x + (index % type) * listView.cellWidth
            items[currentDrag].y = rectBg.y + Math.floor(index / type) * listView.cellHeight

            return true
        }
        else{
            items[currentDrag].x = startX
            items[currentDrag].y = startY

            return false
        }
    }

    function calcOver(){

        var success = false

        for(var i=0;i<listModel.count;i++){
            listView.currentIndex = i
            if(listView.currentItem.myIndex !== i){
                success = false
                break
            }
            if(i == listModel.count - 1){
                success = true
            }
        }

        return success
    }

    function init(){

        timer.restart()
        isZuoBi = false

        imgWidth = Math.floor(name.width / type)
        imgHeight = Math.floor(name.height / type)

        for(var j in items){
            items[j].visible = false
            items[j].destroy()
        }
        items.length = 0
        listModel.clear()

        for(var i=0;i<type*type;i++){

            var path = name.source

            listModel.append({"imgSource": path.toString()})

            var obj = Qt.createComponent("ImgDelegate.qml").createObject(root)
            //1200-1800
            Math.seed = getSeed()
            var x = 1000 + Math.floor(Math.random()*600)
            var y = 100 + Math.floor(Math.random()*700)
            var r = Math.floor(Math.random()*3)


            obj.myIndex = i
            obj.img.source = path.toString()
            obj.x = x
            obj.y = y
            if(r == 0){
                obj.rotation.angle = -45
            }
            else if(r == 1){
                obj.rotation.angle = 45
            }
            else{
                obj.rotation.angle = 0
            }

            items.push(obj)
        }
    }


    function clickLeftList(src){
        name.source = src
    }

    function initLeftList(list){
        leftModel.clear()
        for(var i in list){
            leftModel.append({"imgSource": list[i]})
        }
        name.source = list[0]
    }


    function zuobi(){
        if(isZuoBi){
            for(var i in items){
                items[i].txt.opacity = 0
            }
        }
        else{
            for(var i in items){
                items[i].txt.opacity = 1
            }
        }
        isZuoBi = !isZuoBi
    }

    Component.onCompleted: {
    }

    Connections{
        target: widget
        onInitLeftList: {
            initLeftList(list)
        }
    }

    //update
    Timer{
        id: timer
        interval: 1000
        repeat: true
        onTriggered: {
            if(calcOver()){
                //win
                txtTips.opacity = 1
                anim.restart()
                timer.stop()
            }
        }
    }

    Rectangle{
        anchors.fill: parent
        color: Qt.rgba(1,0,0,0.3)
    }

    //原图
    Image {
        id: name
        x: 10
        y: 10
        width: 400
        height: 400
        fillMode: Image.Stretch

        onSourceChanged: {
            init()
        }
    }

    //拼图
    Rectangle{
        id: rectBg
        y: 10
        x: name.width + 50
        width: (imgWidth + 2)*type
        height: (imgHeight + 2)*type
        color: "lightblue"

        GridView{
            id: listView
            width: parent.width
            height: parent.height
            cellWidth: imgWidth + 2
            cellHeight: imgHeight + 2
            delegate: ImgDelegateGrid{}
            model: ListModel{
                id: listModel
            }
            interactive: false
        }
    }

    //列表
    Rectangle{
        id: rectLeftBg
        x: 20
        y: 450
        width: 100
        height: 500
        color: Qt.rgba(0,0,0,0)
        clip: true

        ListView{
            id: leftListView
            width: 100
            height: 500 + 5
            delegate: DelegateButton{}
            model: ListModel{
                id: leftModel
            }
            spacing: 1
        }
    }

    //难度
    Text {
        id: txtLevel
        x: 150
        y: rectLeftBg.y
        text: "难度："
        font.pixelSize: 40
        font.family: "微软雅黑"
    }
    MyComBox{
        id: combox
        width: 100
        height: 30
        anchors.verticalCenter: txtLevel.verticalCenter
        x: txtLevel.width + txtLevel.x + 10
        onComboClicked: {
            var level = Number(name)
            type = level + 2
            init()
        }
    }

    Button{
        text: "作弊"
        anchors.right: parent.right
        anchors.rightMargin: 10
        y: 10
        onClicked: {
            zuobi()
        }
    }

    Text {
        id: txtTips
        y: 500
        anchors.horizontalCenter: parent.horizontalCenter
        text: "你赢了"
        font.pixelSize: 100
        font.family: "微软雅黑"
        color: "orange"
        opacity: 0
    }

    SequentialAnimation{
        id: anim
        PauseAnimation { duration: 1000}
        PropertyAnimation{target: txtTips; property: "opacity"; from: 1; to: 0; duration: 2000}
    }

}
